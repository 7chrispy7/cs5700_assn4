#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Sudoku_solver/Timer.h"
#include "../Sudoku_solver/Timer.cpp"
#include "../Sudoku_solver/Cell.h"
#include "../Sudoku_solver/Cell.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SudokuSolverTesting
{		
	TEST_CLASS(TimerTesting)
	{
	public:
		
		TEST_METHOD(InitTest)
		{
			Timer timer;
			Assert::AreEqual(0.0, timer.getTotalTime());
		}
		TEST_METHOD(runTimerTest)
		{
			Timer timer;
			timer.start();
			int j = 1;
			for (size_t i = 0; i < 1000000; i++)
			{
				j *= i;
			}
			j++;
			timer.stop();
			double firstTime = timer.getTotalTime();
			Assert::AreNotEqual(0.0, firstTime);
			timer.start();
			for (size_t i = 0; i < 1000000; i++)
			{
				j *= i;
			}
			j++;
			timer.stop();
			Assert::AreNotEqual(firstTime, timer.getTotalTime());
			timer.reset();
			Assert::AreEqual(0.0, timer.getTotalTime());
		}

	};

	TEST_CLASS(CellTesting) {
		TEST_METHOD(InitTest) {
			std::vector<char> vec;
			vec.push_back('1');
			vec.push_back('2');
			vec.push_back('3');
			vec.push_back('4');
			Cell cell(vec,'-');
			for (size_t i = 0; i < vec.size(); i++)
			{
				Assert::AreEqual(cell.possible_values[i], vec[i]);
			}
			Assert::AreEqual(cell.getIsSolved(), false);
			Assert::AreEqual(cell.getValue(), '-');
		}
		TEST_METHOD(RemoveTest) {
			std::vector<char> vec;
			vec.push_back('1');
			vec.push_back('2');
			vec.push_back('3');
			vec.push_back('4');
			Cell cell(vec, '-');
			cell.remove_value('3');
			vec.erase(vec.begin() + 2);
			for (size_t i = 0; i < vec.size(); i++)
			{
				Assert::AreEqual(cell.possible_values[i], vec[i]);
			}
		}
	};
}