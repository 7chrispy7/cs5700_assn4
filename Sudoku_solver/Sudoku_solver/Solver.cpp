#include "Solver.h"

Solver::Solver()
{
	didWork = false;
	timesUsed = 0;
}

Puzzle Solver::processPuzzle(Puzzle & puzzle)
{
	didWork = false;
	timer.start();
	Puzzle newPuzzle = solve(puzzle);
	timer.stop();
	timesUsed++;
	return newPuzzle;
}

void Solver::setCellVal(Cell & cell, const char &value)
{
	cell.setValue(value);
	cell.setIssolved(true);
	didWork = true;
}
