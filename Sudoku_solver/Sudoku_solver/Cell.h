#pragma once
#include <vector>

class Cell {
public:
	Cell(const std::vector<char> values, const char &value, const bool &isSolved = false);
	char getValue() { return value; }
	bool getIsSolved() { return isSolved; }
	void setValue(const char &newValue) { value = newValue; }
	void setIssolved(const bool &newValue) { isSolved = newValue; }
	void remove_value(const char &value);
	
	std::vector<char> possible_values;

private:
	char value;
	bool isSolved;
};