#include <iostream>
#include <iomanip>

#include "UI.h"

int main(int argc,char *argv[]) {
	UI ui;
	std::cout << argv[1] << "\n";
	if (argc == 3)
		ui.mainLoop(argv[1], argv[2]);
	else if (!strcmp(argv[1],"-h")) {
		std::cout << "To use the solver using the command line, please use the following format.\n";
		std::cout << "<Input File Name> <Output File Name>\n\n";
	}
	else if (argc != 1)
		std::cout << "Invalid command line arguments.\n\n";
	else
		ui.mainLoop();

	return 0;
}