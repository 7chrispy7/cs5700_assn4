#include "ShaveSolver.h"
#include "Cell.h"

#include <vector>

Puzzle ShaveSolver::solve(Puzzle & puzzle)
{
	Puzzle newPuzzle = puzzle;
	shaveRows(newPuzzle);
	shaveCols(newPuzzle);
	shaveBlocks(newPuzzle);
	verifyCells(newPuzzle);

	return newPuzzle;
}

void ShaveSolver::shaveRows(Puzzle & puzzle)
{
	for (size_t i = 1; i <= puzzle.getOrder()*puzzle.getOrder(); i++)
	{
		std::vector<Cell*> row = puzzle.getRow(i);
		
		//find solved values
		std::vector<char> solved_vals;
		for (size_t i = 0; i < row.size(); i++)
		{
			if (row[i]->getIsSolved() == true)
				solved_vals.push_back(row[i]->getValue());
		}
		//shave
		for (size_t i = 0; i < row.size(); i++)
		{
			if (row[i]->getIsSolved() == false) {
				for (size_t j = 0; j < solved_vals.size(); j++)
				{
					row[i]->remove_value(solved_vals[j]);
				}
			}
		}
	}
}

void ShaveSolver::shaveCols(Puzzle & puzzle)
{
	for (size_t i = 1; i <= puzzle.getOrder()*puzzle.getOrder(); i++)
	{
		std::vector<Cell*> col = puzzle.getCol(i);

		//find solved values
		std::vector<char> solved_vals;
		for (size_t i = 0; i < col.size(); i++)
		{
			if (col[i]->getIsSolved() == true)
				solved_vals.push_back(col[i]->getValue());
		}
		//shave
		for (size_t i = 0; i < col.size(); i++)
		{
			if (col[i]->getIsSolved() == false) {
				for (size_t j = 0; j < solved_vals.size(); j++)
				{
					col[i]->remove_value(solved_vals[j]);
				}
			}
		}
	}
}

void ShaveSolver::shaveBlocks(Puzzle & puzzle)
{
	for (size_t i = 1; i <= puzzle.getOrder()*puzzle.getOrder(); i++)
	{
		std::vector<Cell*> block = puzzle.getBlock(i);

		//find solved values
		std::vector<char> solved_vals;
		for (size_t i = 0; i < block.size(); i++)
		{
			if (block[i]->getIsSolved() == true)
				solved_vals.push_back(block[i]->getValue());
		}
		//shave
		for (size_t i = 0; i < block.size(); i++)
		{
			if (block[i]->getIsSolved() == false) {
				for (size_t j = 0; j < solved_vals.size(); j++)
				{
					block[i]->remove_value(solved_vals[j]);
				}
			}
		}
	}
}

void ShaveSolver::verifyCells(Puzzle & puzzle)
{
	for (size_t i = 0; i < puzzle.cells.size(); i++)
	{
		if (puzzle.cells[i].possible_values.size() == 1 && puzzle.cells[i].getIsSolved() == false)
			setCellVal(puzzle.cells[i], puzzle.cells[i].possible_values[0]);
	}
}
