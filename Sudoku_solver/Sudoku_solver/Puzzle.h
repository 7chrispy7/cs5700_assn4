#pragma once
#include <string>
#include <vector>

#include "Cell.h"


class Puzzle {
public:
	Puzzle(const std::string &filename);
	std::vector<Cell> cells;

	std::vector<char> getSymbols() { return symbols; }
	int getOrder() { return order; }
	int getIsValid() { return isValid; }
	void setIsValid(const int &errorNumber) { isValid = errorNumber; }
	std::vector<Cell*> getRow(const int &rowNum);
	std::vector<Cell*> getCol(const int &colNum);
	std::vector<Cell*> getBlock(const int &blockNum);
	std::string symbolString();
	std::string getPuzzleVals();
	int cellsToSolve();

private:
	int isValid;	//[0] valid, [-1] incorrect format [-2] invalid dimensions 
					//[-3] invalid symbol [-4] unsolvable [-5] multiple solutions.
	int order;
	std::vector<char> symbols;
};