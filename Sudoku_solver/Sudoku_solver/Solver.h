#pragma once
#include "Timer.h"
#include "Puzzle.h"
#include "Cell.h"

#include <string>


class Solver {
public:
	Solver();
	Puzzle processPuzzle(Puzzle &puzzle);
	bool getDidWork() { return didWork; }
	int getTimesUsed() { return timesUsed; }
	double getTotalTime() { return timer.getTotalTime(); }
	std::string getName() { return name; }
	void setCellVal(Cell &cell, const char &value);

private:
	virtual Puzzle solve(Puzzle &puzzle) = 0;
	Timer timer;
	bool didWork;
	int timesUsed;
protected:
	std::string name;
};