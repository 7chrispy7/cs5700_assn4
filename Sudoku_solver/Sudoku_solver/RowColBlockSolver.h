#pragma once
#include "Solver.h"

#include <vector>

class RowColBlockSolver : public Solver {
public:
	RowColBlockSolver() { name = "Row Column Block"; };
private:
	Puzzle solve(Puzzle &puzzle);
	void solveCells(std::vector<Cell*> cells, std::vector<char> symbolsNeeded);
};