#include "Cell.h"

Cell::Cell(const std::vector<char> values, const char & value, const bool & isSolved)
{
	possible_values = values;
	this->value = value;
	this->isSolved = isSolved;
}

void Cell::remove_value(const char & removeValue)
{
	for (size_t i = 0; i < possible_values.size(); i++)
	{
		if (possible_values[i] == removeValue) {
			possible_values.erase(possible_values.begin()+i);
		}
	}
}
