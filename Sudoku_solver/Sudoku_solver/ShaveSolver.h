#pragma once
#include "Solver.h"

class ShaveSolver : public Solver {
public:
	ShaveSolver() {	name = "Shave (Only One Possible)";	}
private:
	Puzzle solve(Puzzle &puzzle);
	void shaveRows(Puzzle &puzzle);
	void shaveCols(Puzzle &puzzle);
	void shaveBlocks(Puzzle &puzzle);
	void verifyCells(Puzzle &puzzle);
};