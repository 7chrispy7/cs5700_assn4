#include "Puzzle.h"

#include <fstream>
#include <iostream>

Puzzle::Puzzle(const std::string & filename)
{
	isValid = 0;
	int temp;

	std::ifstream instream(filename);
	instream >> temp;
	order = sqrt(temp);
	if (order*order != temp) {
		isValid = -2;
	}

	for (size_t i = 0; i < temp; i++)
	{
		char newChar;
		instream >> newChar;
		symbols.push_back(newChar);
	}

	char cellVal;
	int index = 0;
	while (instream >> cellVal)
	{
		if (cellVal == '-') {
			Cell cell(symbols, cellVal);
			cells.push_back(cell);
		}
		else {
			if (std::find(symbols.begin(), symbols.end(), cellVal) == symbols.end())
				isValid = -3;
			Cell cell(std::vector<char>(cellVal), cellVal, true);
			cells.push_back(cell);
		}
		index++;
	}
	if (index != temp * temp)
		isValid = -1;
}

std::vector<Cell*> Puzzle::getRow(const int &rowNum)
{
	std::vector<Cell*> toRet;
	if (rowNum > 0 && rowNum <= order * order) {
		int cellsPerRow = order * order;
		for (size_t i = cellsPerRow * (rowNum - 1); i < cellsPerRow*rowNum; i++)
		{
			toRet.push_back(&cells[i]);
		}
	}
	else {
		std::cout << "Invalid row requested.\n";
	}
	return toRet;
}

std::vector<Cell*> Puzzle::getCol(const int &colNum)
{
	std::vector<Cell*> toRet;
	if (colNum > 0 && colNum <= order * order) {
		int cellsPerCol = order * order;
		for (size_t i = colNum - 1; i < (cellsPerCol*(cellsPerCol-1))+colNum; i+=cellsPerCol)
		{
			toRet.push_back(&cells[i]);
		}
	}
	else {
		std::cout << "Invalid col requested.\n";
	}
	return toRet;
}

std::vector<Cell*> Puzzle::getBlock(const int & blockNum)
{
	std::vector<Cell*> toRet;
	if (blockNum > 0 && blockNum <= order * order) {
		int cellsPerBlock = order * order;
		int firstVerticalCell = ((blockNum-1) / order) * cellsPerBlock*order;
		for (size_t i = firstVerticalCell; i < firstVerticalCell + order*cellsPerBlock; i += cellsPerBlock)
		{
			for (size_t j = ((blockNum-1) % order) * order; j < (((blockNum-1) % order)*order)+order; j ++)
			{
				toRet.push_back(&cells[i+j]);
			}
		}
	}
	else {
		std::cout << "Invalid block requested.\n";
	}
	return toRet;
}

std::string Puzzle::symbolString()
{
	std::string toRet;
	for (size_t i = 0; i < symbols.size(); i++)
	{
		toRet += symbols[i];
		toRet += ' ';
	}
	return toRet;
}

std::string Puzzle::getPuzzleVals()
{
	std::string toRet;
	for (size_t i = 0; i < cells.size(); i++)
	{
		toRet += cells[i].getValue();
		toRet += ' ';
		if ((i+1) % (order*order) == 0 && i != 1) {
			toRet += '\n';
		}
	}
	return toRet;
}

int Puzzle::cellsToSolve()
{
	int toRet = 0;
	for (size_t i = 0; i < cells.size(); i++)
	{
		if (cells[i].getIsSolved() == false)
			toRet++;
	}
	return toRet;
}
