#include "BacktrackSolver.h"

#include <iostream>

BacktrackSolver::BacktrackSolver()
{
	name = "Backtrack";
	cellIndex = 0;
	firstUnsolvedCell = -1;
}

Puzzle BacktrackSolver::solve(Puzzle & puzzle)
{
	Puzzle toRet = puzzle;
	init(puzzle);
	int orderSq = puzzle.getOrder()*puzzle.getOrder();
	cellIndex = firstUnsolvedCell;

	while (cellIndex >= firstUnsolvedCell) {
		if (!toRet.cells[cellIndex].getIsSolved()) {
			if (nextVal(toRet.cells[cellIndex])) {
				if (checkVal(toRet.cells[cellIndex], toRet)) {
					nextCell();
				}
			}
			else
				prevCell(toRet.cells[cellIndex]);
		}
		else
			nextCell();
		if (cellIndex >= orderSq * orderSq) {
			solutions.push_back(toRet);
			cellIndex--;
			prevCell(toRet.cells[cellIndex]);
		}
	}
	if (cellIndex < firstUnsolvedCell && solutions.size() == 0) {
		puzzle.setIsValid(-4);
	}
	return puzzle;
}

void BacktrackSolver::init(Puzzle & puzzle)
{
	for (size_t i = 0; i < puzzle.cells.size(); i++)
	{
		if (puzzle.cells[i].getIsSolved()) {
			valueIndex.push_back(-2);
		}
		else {
			valueIndex.push_back(-1);
			if (firstUnsolvedCell == -1)
				firstUnsolvedCell = i;
		}
	}
}

void BacktrackSolver::nextCell()
{
	cellIndex++;
}

void BacktrackSolver::prevCell(Cell &cell)
{
	cell.setValue('-');
	if(valueIndex[cellIndex] != -2)
		valueIndex[cellIndex] = -1;

	do {
		cellIndex--;
	} while (valueIndex[cellIndex] == -2 && cellIndex >= firstUnsolvedCell);
}

bool BacktrackSolver::nextVal(Cell & cell)
{
	valueIndex[cellIndex]++;
	if (valueIndex[cellIndex] < cell.possible_values.size()) {
		cell.setValue(cell.possible_values[valueIndex[cellIndex]]);
		return true;
	}
	return false;
}

bool BacktrackSolver::checkVal(Cell & cell, Puzzle & puzzle)
{
	int orderSq = puzzle.getOrder()*puzzle.getOrder();
	int rowNum = ((cellIndex) / orderSq) + 1;
	int colNum = (cellIndex) % orderSq + 1;
	int blockNum = puzzle.getOrder()*((rowNum - 1) / puzzle.getOrder()) + ((colNum - 1) / puzzle.getOrder()) + 1;

	std::vector<Cell*> row = puzzle.getRow(rowNum);
	std::vector<Cell*> col = puzzle.getCol(colNum);
	std::vector<Cell*> block = puzzle.getBlock(blockNum);
	char cellVal = cell.getValue();

	for (size_t i = 0; i < row.size(); i++)
	{
		if (&cell != row[i] && cellVal == row[i]->getValue()) {
			return false;
		}
	}
	for (size_t i = 0; i < col.size(); i++)
	{
		if (&cell != col[i] && cellVal == col[i]->getValue()) {
			return false;
		}
	}for (size_t i = 0; i < block.size(); i++)
	{
		if (&cell != block[i] && cellVal == block[i]->getValue()) {
			return false;
		}
	}

	return true;
}
