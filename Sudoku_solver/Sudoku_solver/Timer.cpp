#include "Timer.h"

Timer::Timer()
{
	total_time = 0.0;
}

void Timer::start()
{
	startTime = std::clock();
}

void Timer::stop()
{
	//total time in ms
	endTime = std::clock();
	total_time += 1000.0 * (endTime - startTime) / CLOCKS_PER_SEC;
}
