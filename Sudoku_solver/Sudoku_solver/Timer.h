#pragma once
#include <string>
#include <ctime>

class Timer {
public:
	Timer();
	void start();
	void stop();
	double getTotalTime() { return total_time; }
	void reset() { total_time = 0; }
private:
	double total_time;
	clock_t startTime;
	clock_t endTime;
};