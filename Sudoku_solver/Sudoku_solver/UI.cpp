#include "UI.h"
#include "Puzzle.h"
#include "ShaveSolver.h"
#include "RowColBlockSolver.h"
#include "BacktrackSolver.h"

#include <iostream>
#include <iomanip>
#include <fstream>

void UI::mainLoop(std::string fileIn, std::string fileOut)
{
	if (fileIn != "" && fileOut != "") {
		Done = true;
		solveAndExport(fileIn, fileOut);
	}
	std::cout << "Welcome to the Sudoku Solver!\n";
	while (!Done) {
		std::cout << "Please select an option:\n";
		std::cout << "\t[1]Solve a puzzle\n";
		std::cout << "\t[2]Exit\n";
		char userSelection;
		std::cin >> userSelection;
		switch (userSelection)
		{
		case '1':
		{
			std::cout << "Please select a sudoku puzzle file: ";
			std::cin >> fileIn;
			std::cout << "Please select a puzzle output file: ";
			std::cin >> fileOut;
			solveAndExport(fileIn, fileOut);
			break;
		}
		case '2':
			Done = true;
			break;
		case '0':
			//testing mode
			solveAndExport("../../SamplePuzzles/Input/Puzzle-4x4-0001.txt", "../../SamplePuzzles/OutputTests/Puzzle-4x4-0001.txt");
			break;
		default:
			std::cout << "That is not a valid option.\n\n";
			break;
		}
	}
}

void UI::solveAndExport(const std::string & fileIn, const std::string & fileOut)
{
	std::cout << "\nProcessing puzzle...\n\n";
	Puzzle puzzle(fileIn);
	std::ofstream outstream;
	outstream.open(fileOut);

	//Output the unsolved puzzle information
	outstream << puzzle.getOrder()*puzzle.getOrder() << "\n";
	outstream << puzzle.symbolString() << "\n" << puzzle.getPuzzleVals() << "\n";

	//Account for errors
	if (puzzle.getIsValid() != 0) {
		//TODO: Custom messages for different bad puzzles.
		outstream << "Invalid: ";
		if (puzzle.getIsValid() == -1)
			outstream << "Incorrect Format\n\n";
		else if (puzzle.getIsValid() == -2)
			outstream << "Invalid Dimensions\n\n";
		else if (puzzle.getIsValid() == -3)
			outstream << "Inappropriate Symbol\n\n";
	}
	else {
		ShaveSolver shavesolver;
		RowColBlockSolver rcbsolver;
		BacktrackSolver backtracksolver;

		/*do {
			puzzle = shavesolver.processPuzzle(puzzle);
			if(puzzle.cellsToSolve() != 0)
				puzzle = rcbsolver.processPuzzle(puzzle);
		}
		while ((shavesolver.getDidWork() || rcbsolver.getDidWork()) && puzzle.cellsToSolve() != 0);
	*/
		if (puzzle.cellsToSolve() != 0) {
			backtracksolver.processPuzzle(puzzle);
		}

		if (puzzle.cellsToSolve() == 0) {
			outstream << "Solved:\n";
			outstream << puzzle.getPuzzleVals() << "\n\n";
		}
		else if (backtracksolver.solutions.size() == 0) {
			outstream << "Unsolvable\n\n";
		}
		else if (backtracksolver.solutions.size() == 1) {
			outstream << "Solved:\n";
			outstream << backtracksolver.solutions[0].getPuzzleVals() << "\n\n";
		}
		else {
			outstream << "Multiple Solutions:\n";
			for (size_t i = 0; i < backtracksolver.solutions.size(); i++)
			{
				outstream << backtracksolver.solutions[i].getPuzzleVals() << "\n\n";
			}
		}
		outstream << "Strategy\t\t\t\t\t\t" << "Uses\t" << "Time\n";
		outstream << std::fixed << std::setprecision(2);
		outstream << shavesolver.getName() << "\t\t" << shavesolver.getTimesUsed() << "\t\t" << shavesolver.getTotalTime() << " ms\n";
		outstream << rcbsolver.getName() << "\t\t\t\t" << rcbsolver.getTimesUsed() << "\t\t" << rcbsolver.getTotalTime() << " ms\n";
		outstream << backtracksolver.getName() << "\t\t\t\t\t\t" << backtracksolver.getTimesUsed() << "\t\t" << backtracksolver.getTotalTime() << " ms\n";

	}

	outstream.close();
	std::cout << "Solution saved to " << fileOut << "\n\n";
}
