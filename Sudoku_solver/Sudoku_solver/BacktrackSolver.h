#pragma once
#include "Solver.h"

#include <vector>

class BacktrackSolver :public Solver {
public:
	BacktrackSolver();
	std::vector<Puzzle> solutions;
private:
	int cellIndex;
	int firstUnsolvedCell;
	std::vector<int> valueIndex;  //[-2]If cell is solved, else start at -1 and increment.

	Puzzle solve(Puzzle &puzzle);
	void init(Puzzle &puzzle);
	void nextCell();
	void prevCell(Cell &cell);
	bool nextVal(Cell &cell);
	bool checkVal(Cell &cell, Puzzle &puzzle);
};