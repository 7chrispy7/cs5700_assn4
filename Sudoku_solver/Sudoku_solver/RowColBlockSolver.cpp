#include "RowColBlockSolver.h"

Puzzle RowColBlockSolver::solve(Puzzle & puzzle)
{
	std::vector<char> symbolsNeeded = puzzle.getSymbols();

	for (size_t i = 1; i <= puzzle.getOrder()*puzzle.getOrder(); i++)
	{
		solveCells(puzzle.getRow(i), symbolsNeeded);
	}
	for (size_t i = 1; i <= puzzle.getOrder()*puzzle.getOrder(); i++)
	{
		solveCells(puzzle.getCol(i), symbolsNeeded);
	}
	for (size_t i = 1; i <= puzzle.getOrder()*puzzle.getOrder(); i++)
	{
		solveCells(puzzle.getBlock(i), symbolsNeeded);
	}

	return puzzle;
}

void RowColBlockSolver::solveCells(std::vector<Cell*> cells, std::vector<char> symbolsNeeded)
{
	for (uint16_t i = 0; i < cells.size(); i++)
	{
		if (cells[i]->getIsSolved()) {
			for (size_t j = 0; j < symbolsNeeded.size(); j++)
			{
				if (symbolsNeeded[j] == cells[i]->getValue())
					symbolsNeeded.erase(symbolsNeeded.begin() + j);
			}
		}
	}

	for (size_t i = 0; i < symbolsNeeded.size(); i++)
	{
		bool onlyOneCell = true;
		int insertIndex = -1;
		for (size_t j = 0; j < cells.size(); j++)
		{
			if (!cells[j]->getIsSolved()) {
				for (size_t k = 0; k < cells[j]->possible_values.size(); k++)
				{
					if (symbolsNeeded[i] == cells[j]->possible_values[k]) {
						if (insertIndex == -1) {
							insertIndex = k;
						}
						else {
							onlyOneCell = false;
						}
					}
				}
			}
		}
		if (onlyOneCell && insertIndex != -1) {
			setCellVal(*cells[insertIndex],symbolsNeeded[i]);
		}
	}
}
