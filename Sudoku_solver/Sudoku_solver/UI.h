#pragma once
#include <string>

class UI {
public:
	void mainLoop(std::string fileIn = "", std::string fileOut = "");
private:
	void solveAndExport(const std::string &fileIn, const std::string &fileOut);
	bool Done = false;
};